<?php
// This file is part of POAS Moodle Plugins - https://bitbucket.org/oasychev/moodle-plugins-other
//
// POAS Moodle Plugins is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// POAS Moodle Plugins is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with POAS Moodle Plugins.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version file for skipsubmit theme.
 *
 * @package   theme_skipsubmit
 * @copyright 2016 Yuriy Osychenko <yuriy.osychenko@gmail.com>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

// This is the version of the plugin.
$plugin->version = '2016120500';

// This is the version of Moodle this plugin requires.
$plugin->requires = '2016112900';

// This is the component name of the plugin - it always starts with 'theme_'
// for themes and should be the same as the name of the folder.
$plugin->component = 'theme_skipsubmit';

// This is a list of plugins, this plugin depends on (and their versions).
$plugin->dependencies = [
    'theme_clean' => '2016120500',
    'theme_bootstrapbase' => '2016120500',
    'mod_quiz' => '2016120500'
];