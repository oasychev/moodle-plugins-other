<?php
// This file is part of POAS Moodle Plugins - https://bitbucket.org/oasychev/moodle-plugins-other
//
// POAS Moodle Plugins is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// POAS Moodle Plugins is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with POAS Moodle Plugins.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Basic language file for skipsubmit theme.
 *
 * @package   theme_skipsubmit
 * @copyright 2016 Yuriy Osychenko <yuriy.osychenko@gmail.com>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['configtitle'] = 'Skip & Submit';

$string['customcss'] = 'Custom CSS';
$string['customcssdesc'] = 'Whatever CSS rules you add to this textarea will be reflected in every page, making for easier customization of this theme.';

$string['footnote'] = 'Footnote';
$string['footnotedesc'] = 'Whatever you add to this textarea will be displayed in the footer throughout your Moodle site.';

$string['invert'] = 'Invert navbar';
$string['invertdesc'] = 'Swaps text and background for the navbar at the top of the page between black and white.';

$string['logo'] = 'Logo';
$string['logodesc'] = 'The logo is only displayed in the header of the front page and login page.<br /> If the height of your logo is more than 75px add div.logo {height: 100px;} to the Custom CSS box below, amending accordingly if the height is other than 100px. If a logo is not added here, the logo in Appearance > Logos will be used.';

// A description shown in the admin theme selector.
$string['choosereadme'] = 'Theme Skip & Submit is a child theme of Clean. It adds the ability to skip and submit attempt from each page.';

$string['pluginname'] = 'Skip & Submit';

$string['region-side-post'] = 'Right';
$string['region-side-pre'] = 'Left';

$string['sitelogo'] = 'Site logo';
$string['sitename'] = 'Display site name along with small logo';
$string['sitenamedesc'] = 'If there is no small logo, the site name is always displayed in the navigation bar. If a small logo is set, it may be displayed with or without the site name.';
$string['smalllogo'] = 'Small logo';
$string['smalllogodesc'] = 'The small logo is displayed in the navigation bar. If there is a header logo for the front page and login page, the small logo is not displayed on these pages. If a logo is not added here, the compact logo in Appearance > Logos will be used.';
