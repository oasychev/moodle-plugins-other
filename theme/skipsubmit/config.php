<?php
// This file is part of POAS Moodle Plugins - https://bitbucket.org/oasychev/moodle-plugins-other
//
// POAS Moodle Plugins is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// POAS Moodle Plugins is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with POAS Moodle Plugins.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Configuration file for skipsubmit theme.
 *
 * @package   theme_skipsubmit
 * @copyright 2016 Yuriy Osychenko <yuriy.osychenko@gmail.com>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

// The name of the theme.
$THEME->name = 'skipsubmit';

// This setting list the style sheets we want to include in our theme.
$THEME->sheets = [];

// This is a setting that can be used to provide some styling to the content in the TinyMCE text editor.
$THEME->editor_sheets = [];

// Parent theme.
$THEME->parents = ['bootstrapbase', 'clean'];

// A dock is a way to take blocks out of the page and put them in a persistent floating area on the side of the page.
$THEME->enable_dock = true;

// This is an old setting used to load specific CSS for some YUI JS.
$THEME->yuicssmodules = [];

// Most themes will use this rendererfactory as this is the one that allows the theme to override any other renderer.
$THEME->rendererfactory = 'theme_overridden_renderer_factory';
$THEME->csspostprocess = 'theme_skipsubmit_process_css';