<?php
// This file is part of POAS Moodle Plugins - https://bitbucket.org/oasychev/moodle-plugins-other
//
// POAS Moodle Plugins is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// POAS Moodle Plugins is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with POAS Moodle Plugins.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Overriden mod_quiz_renderer class.
 *
 * @package   theme_skipsubmit
 * @copyright 2016 Yuriy Osychenko <yuriy.osychenko@gmail.com>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * This override inserts "Submit all and finish" button into attempt form.
 * "Submit all and finish" button makes possible for a student to finish attempt from each page of the quiz.
 */
class theme_skipsubmit_mod_quiz_renderer extends mod_quiz_renderer {

    /**
     * Ouputs the form for making an attempt
     *
     * @param quiz_attempt $attemptobj
     * @param int $page Current page number
     * @param array $slots Array of integers relating to questions
     * @param int $id ID of the attempt
     * @param int $nextpage Next page number
     */
    public function attempt_form($attemptobj, $page, $slots, $id, $nextpage) {
        $output = '';

        // Outputs HTML for attempt form from parent method.
        $output .= parent::attempt_form($attemptobj, $page, $slots, $id, $nextpage);

        // Finds position of the last closing </form> tag.
        $insertpos = strrpos($output, "</form>");

        // Generates HTML for "Submit all and finish" button.
        $submitbuttonstr = html_writer::empty_tag('input', array('type' => 'submit', 'name' => 'finishattempt',
                    'value' => get_string('submitallandfinish', 'quiz'), 'id' => 'submit-and-finish-button'));

        // Inserts "Submit all and finish" button before last closing attempt form tag.
        $output = substr_replace($output, $submitbuttonstr, $insertpos, 0);

        // Defines onClick handler for "Submit all and finish" button.
        $this->page->requires->event_handler('#submit-and-finish-button', 'click', 'M.util.show_confirm_dialog',
                    array('message' => get_string('confirmclose', 'quiz')));
        return $output;
    }
}