<?php
// This file is part of ajaxcategories plugin - https://code.google.com/p/oasychev-moodle-plugins/
//
// Ajaxcategories plugin is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Defines classes of list and list items for ajax categories.
 *
 * @package    local_ajaxcategories
 * @copyright  2015 Oleg Sychev, Volgograd State Technical University
 * @author     Elena Lepilkina
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

// Number of categories to display on page.

require_once($CFG->libdir.'/listlib.php');
require_once($CFG->dirroot.'/question/move_form.php');
require_once($CFG->dirroot.'/question/category_class.php');
require_once($CFG->dirroot.'/local/ajaxcategories/category_form.php');

/**
 * Class representing a list of ajax question categories
 */
class ajax_question_category_list extends moodle_list {
    /**
     * @var name of table in database.
     */
    public $table = "question_categories";
    /**
     * @var name of list items class.
     */
    public $listitemclassname = 'ajax_question_category_list_item';
    /**
     * @var reference to list displayed below this one.
     */
    public $nextlist = null;
    /**
     * @var reference to list displayed above this one.
     */
    public $lastlist = null;
    /**
     * @var context og this list.
     */
    public $context = null;
    /**
     * @var columns for sorting.
     */
    public $sortby = 'parent, sortorder, name';

    public function __construct($type='ul', $attributes='', $editable = false, $pageurl=null, $page = 0,
                                $pageparamname = 'page', $itemsperpage = 20, $context = null) {
        parent::__construct('ul', $attributes, $editable, $pageurl, $page, 'cpage', $itemsperpage);
        $this->context = $context;
    }

    /**
     * Returns records from list.
     *
     * @return list records.
     */
    public function get_records() {
        $this->records = get_categories_for_contexts($this->context->id, $this->sortby);
    }

    /**
     * Replace category item in choosen place.
     *
     * @param $movingid - id of question_category_item replacing category.
     * @param $environment - array with keys: 'before' - before item id
     *                                      'after' - after item id
     *                                      'level' - 'normal' or 'inner'
     *                                      'dest' - destination list.
     */
    public function change_category_list($movingid, $environment) {
        global $DB;
        // Get item by id.
        $item = $this->find_item($movingid);
        // Change context.
        if ($environment['dest']->context->id != $this->context->id) {
            // Get all children items of moving item.
            $children = $item->get_all_children();
            $children[] = $movingid;
            // Change context for all replacing categories.
            foreach ($children as $child) {
                $oldcat = $DB->get_record('question_categories', array('id' => $child), '*', MUST_EXIST);
                $oldcat->contextid = $environment['dest']->context->id;
                $DB->update_record('question_categories', $oldcat);
            }
        }

        // Define the place of replacing.
        if ($environment['after'] != -1) {
            // Replacing at the same level after some item.
            $afteritem = $environment['dest']->find_item($environment['after']);
            if ($environment['level'] != "inner") {
                // At the same level.
                if (isset($afteritem->parentlist->parentitem)) {
                    $newparent = $afteritem->parentlist->parentitem->id;
                } else {
                    $newparent = 0;
                }

                $DB->set_field($environment['dest']->table, "parent", $newparent, array("id" => $item->id));
                $newpeers = $environment['dest']->get_items_peers($afteritem->id);
                // Place of item after which should be added moving category.
                $oldkey = array_search($afteritem->id, $newpeers);
                // Place of moving category.
                $key = array_search($item->id, $newpeers);
                // If moving category was at the same list reoder categories in list.
                if ($key) {
                    // Replace moving category up.
                    if ($oldkey < $key) {
                        $neworder = array_merge(array_slice($newpeers, 0, $oldkey + 1), array($item->id));
                        $left = array_slice($newpeers, $oldkey + 1);
                        $keyleft = array_search($item->id, $left);
                        unset($left[$keyleft]);
                        $left = array_values($left);
                        $neworder = array_merge($neworder, $left);
                    } else {
                        // Replace moving category down.
                        $neworder = array_merge(array_slice($newpeers, 0, $key), array_slice($newpeers, $key + 1, $oldkey),
                                                array($item->id), array_slice($newpeers, $oldkey + 1));
                    }
                } else {
                    $neworder = array_merge(array_slice($newpeers, 0, $oldkey + 1), array($item->id),
                                            array_slice($newpeers, $oldkey + 1));
                }
                // Set new order of categories.
                $environment['dest']->reorder_peers($neworder);
            } else {
                // Create new nested list. Replace moving category to it.
                $newlist = new ajax_question_category_list($this->type, $this->attributes, $this->editable, $this->pageurl,
                                                           $this->page, $this->pageparamname, $this->itemsperpage, $this->context);
                $newlist->parentitem = $afteritem;
                $newparent = $afteritem->id;
                $DB->set_field($environment['dest']->table, "parent", $newparent, array("id" => $item->id));
            }
        } else {
            // Replacing at the same level before some item.
            $beforeitem = $environment['dest']->find_item($environment['before']);
            if (isset($beforeitem->parentlist->parentitem)) {
                $newparent = $beforeitem->parentlist->parentitem->id;
            } else {
                $newparent = 0;
            }
            $DB->set_field($environment['dest']->table, "parent", $newparent, array("id" => $item->id));
            $newpeers = $environment['dest']->get_items_peers($beforeitem->id);
            // Place of item after which should be added moving category.
            $oldkey = array_search($beforeitem->id, $newpeers);
            // Place of moving category.
            $key = array_search($item->id, $newpeers);
            // If moving category was at the same list reoder categories in list.
            if ($key) {
                $neworder = array_merge(array_slice($newpeers, 0, $oldkey), array($item->id),
                                        array_slice($newpeers, $oldkey, $key), array_slice($newpeers, $key + 1));
            } else {
                $neworder = array_merge(array_slice($newpeers, 0, $oldkey), array($item->id), array_slice($newpeers, $oldkey));
            }
            $environment['dest']->reorder_peers($neworder);
        }
    }


    /**
     * Returns html string.
     *
     * @param integer $indent depth of indentation.
     * @param $extraargs extra arguments for getting html.
     *
     * @return html code.
     */
    public function to_html($indent = 0, $extraargs = array()) {
        $attributes = array(
            'id' => 'ajaxlistitem',
        );

        $placeholder = array(
            'id' => 'ajaxcategories-placeholder',
        );

        if (count($this->items)) {
            $tabs = str_repeat("\t", $indent);
            $first = true;
            $itemiter = 1;
            $lastitem = '';
            $html = '';
            // Get html string for each list.
            foreach ($this->items as $item) {
                // Create item attributes.
                $itemattributes = array(
                    'id' => 'ajaxitem',
                    'data-id' => $item->id,
                    'order' => $itemiter,
                );
                // Add first placeholder for first item.
                if ($first && $indent !== 0) {
                    $html .= html_writer::start_tag('div', $placeholder);
                    $html .= html_writer::end_tag('div');
                }
                // Write item html.
                $html .= html_writer::start_tag('li', $attributes);
                $html .= html_writer::start_div('ajaxitem', $itemattributes);
                $last = (count($this->items) == $itemiter);
                if ($this->editable) {
                    $item->set_icon_html($first, $last, $lastitem);
                }
                if ($itemhtml = $item->to_html($indent + 1, $extraargs)) {
                    $html .= $itemhtml;
                }

                $html .= html_writer::end_tag('li');
                
                if ($indent !== 0) {
                    $html .= html_writer::start_tag('div', $placeholder);
                    $html .= html_writer::end_tag('div');
                }
                
                $first = false;
                $lastitem = $item;
                $itemiter++;
            }
        } else {
            $html = '';
        }
        if ($html) {// If there are list items to display then wrap them in ul / ol tag.
            $tabs = str_repeat("\t", $indent);
            $html = $tabs.'<'.$this->type.((!empty($this->attributes)) ? (' '.$this->attributes) : '').">\n".$html;
            $html .= $tabs."</".$this->type.">\n";
        } else {
            $html = '';
        }
        return $html;
    }
}


/**
 * An item in a list of question categories.
 *
 */
class ajax_question_category_list_item extends question_category_list_item {
    /**
     * Returns array of all children items of this.
     *
     * @return array of all children items.
     */
    public function get_all_children() {
        $children = array();
        if (isset($this->children)) {
            if (isset($this->children->items)) {
                foreach ($this->children->items as $item) {
                    $children[] = $item->id;
                    $children = array_merge($children, $item->get_all_children());
                }
            }
        }
        return $children;
    }

    public function image_icon($action, $url, $icon) {
        global $OUTPUT;
        $img = html_writer::img($OUTPUT->image_url('t/'.$icon), s($action), array('class' => 'iconsmall'));
        $link = html_writer::link($url, $img, array('title' => s($action), 'class' => 'editcatlink'));
        return $link;
    }

    /**
     * Generate html code for icons.
     */
    public function set_icon_html($first, $last, $lastitem) {
        global $CFG;
        $category = $this->item;
        $url = new moodle_url('/local/ajaxcategories/index.php',
            ($this->parentlist->pageurl->params() + array('edit' => $category->id)));
        $this->icons['edit'] = $this->image_icon(get_string('editthiscategory', 'question'), $url, 'edit');
    }

    /**
     * Returns html
     *
     * @param integer $indent
     * @param array $extraargs any extra data that is needed to print the list item
     *                            may be used by sub class.
     * @return string html
     */
    public function to_html($indent = 0, $extraargs = array()) {
        if (!$this->display) {
            return '';
        }
        $tabs = str_repeat("\t", $indent);

        if (isset($this->children)) {
            $childrenhtml = $this->children->to_html($indent + 1, $extraargs);
        } else {
            $childrenhtml = '';
        }
        return $this->item_html($extraargs).'&nbsp;'.(join($this->icons, '')) .
               html_writer::end_div() . (($childrenhtml != '') ? ("\n".$childrenhtml) : '');
    }

    /**
     * Return html for item.
     *
     * @param array $extraargs any extra data that is needed to print the list item
     *                            may be used by sub class.
     */
    public function item_html($extraargs = array()) {
        global $CFG, $OUTPUT;
        $str = $extraargs['str'];
        $category = $this->item;
        $editqestions = get_string('editquestions', 'question');

        // Each section adds html to be displayed as part of this list item.
        $questionbankurl = new moodle_url('/question/edit.php', $this->parentlist->pageurl->params());
        $questionbankurl->param('cat', $category->id . ',' . $category->contextid);
        $catediturl = new moodle_url($this->parentlist->pageurl, array('edit' => $this->id));
        $item = '';
        // Add drag-handle icon.
        if ($this->parentlist !== null && ($this->parentlist->parentitem !== null || count($this->parentlist->items) > 1)) {
            $item .= html_writer::div($OUTPUT->pix_icon('i/move_2d',
                'You can drag and drop this category'), 'ajaxcategories-drag-handle');
        }
        $item .= html_writer::tag('b', html_writer::link($catediturl,
                format_string(' ' . $category->name, true, array('context' => $this->parentlist->context)),
                array('title' => $str->edit, 'class' => 'editcatlink'))) . ' ';
        $item .= html_writer::link($questionbankurl, '(' . $category->questioncount . ')',
                array('title' => $editqestions)) . ' ';
        $item .= format_text($category->info, $category->infoformat,
                array('context' => $this->parentlist->context, 'noclean' => true));

        // Don't allow delete if this is the last category in this context.
        if (count($this->parentlist->records) != 1) {
            $deleteurl = new moodle_url($this->parentlist->pageurl, array('delete' => $this->id, 'sesskey' => sesskey()));
            $item .= html_writer::link($deleteurl,
                    html_writer::empty_tag('img', array('src' => $OUTPUT->image_url('t/delete'),
                            'class' => 'iconsmall', 'alt' => $str->delete)),
                    array('title' => $str->delete));
        }
        return $item;
    }
}


/**
 * Class representing q question category
 */
class ajax_question_category_object extends question_category_object {

    /**
     * @var ajax_question_category_edit_form Object representing form for adding / editing categories with ajax.
     */
    public $ajaxcatform;

    /**
     * Initializes this classes general category-related variables.
     *
     * @param $page html page.
     * @param $contexts contexts in question bank.
     * @param $currentcat current category.
     * @param $defaultcategory default category.
     * @param $todelete what do to delete.
     * @param $addcontexts adding contexts.
     */
    public function initialize($page, $contexts, $currentcat, $defaultcategory, $todelete, $addcontexts) {
        $lastlist = null;
        foreach ($contexts as $context) {
            $this->editlists[$context->id] = new ajax_question_category_list('ul', 'id="ajaxcategorylist" data-id = "' .
                                                                             $context->id . '"', true, $this->pageurl, $page,
                                                                             'cpage', QUESTION_PAGE_LENGTH, $context);
            $this->editlists[$context->id]->lastlist =& $lastlist;
            if ($lastlist !== null) {
                $lastlist->nextlist =& $this->editlists[$context->id];
            }
            $lastlist =& $this->editlists[$context->id];
        }

        $count = 1;
        $paged = false;
        foreach ($this->editlists as $key => $list) {
            list($paged, $count) = $this->editlists[$key]->list_from_records($paged, $count);
        }
        $this->ajaxcatform = new ajax_question_category_edit_form($this->pageurl, compact('contexts', 'currentcat'));
        if (!$currentcat) {
            $this->ajaxcatform->set_data(array('parent' => $defaultcategory));
        }
    }

    /**
     * Returns list with category.
     *
     * @param integer $categoryid id of category which should be in searched list.
     */
    public function find_list($categoryid) {
        foreach ($this->editlists as $key => $list) {
            if ($list->find_item($categoryid, true) !== null) {
                return $list;
            }
        }
        return null;
    }

    /**
     * Outputs a list to allow editing/rearranging of existing categories
     *
     * $this->initialize() must have already been called
     *
     */
    public function output_edit_lists_html() {
        global $OUTPUT;
        $htmlcode = '';
        $htmlcode .= $OUTPUT->heading_with_help(get_string('editcategories', 'question'), 'editcategories', 'question');
        $htmlcode .= html_writer::start_div('editlists');
        foreach ($this->editlists as $context => $list) {
            $listhtml = $list->to_html(0, array('str' => $this->str));
            if ($listhtml) {
                $htmlcode .= $OUTPUT->box_start('boxwidthwide boxaligncenter generalbox questioncategories contextlevel'
                    . $list->context->contextlevel);
                $fullcontext = context::instance_by_id($context);
                $htmlcode .= html_writer::tag("h3", get_string('questioncatsfor', 'question', $fullcontext->get_context_name()));
                $htmlcode .= $listhtml;
                $htmlcode .= $OUTPUT->box_end();
            }
        }
        $htmlcode .= html_writer::end_div();
        $htmlcode .= $list->display_page_numbers();
        return $htmlcode;
    }

    /**
     * Display the user interface.
     *
     */
    public function display_user_interface() {

        // Interface for editing existing categories.
        $this->output_edit_lists();

        // Interface for adding a new category.
        echo '<br />';
        echo html_writer::start_div('ajaxcatform');
        $this->ajaxcatform->display();
        echo html_writer::end_div();
        // Button to show adding form.
        $button = html_writer::link("#", get_string('addcategory', 'question'),
            array('class' => 'btn btn-primary', 'id' => 'btn-show-form'));
        echo $button;
        echo '<br />';
    }

    /**
     * Customize outputs a list to allow editing/rearranging of existing categories
     *
     * $this->initialize() must have already been called
     *
     */
    public function output_edit_lists() {
        global $OUTPUT;

        echo $OUTPUT->heading_with_help(get_string('editcategories', 'question'), 'editcategories', 'question');
        echo html_writer::start_div('editlists');
        foreach ($this->editlists as $context => $list) {
            $listhtml = $list->to_html(0, array('str' => $this->str));
            if ($listhtml) {
                echo $OUTPUT->box_start('boxwidthwide boxaligncenter generalbox questioncategories contextlevel'
                    .$list->context->contextlevel);
                $fullcontext = context::instance_by_id($context);
                echo $OUTPUT->heading(get_string('questioncatsfor', 'question', $fullcontext->get_context_name()), 3);
                echo $listhtml;
                echo $OUTPUT->box_end();
            }
        }
        echo html_writer::end_div();
        echo $list->display_page_numbers();
    }

    /**
     * Updates an existing category with given params with return json object
     */
    public function ajax_update_category($updateid, $newparent, $newname, $newinfo, $newinfoformat = FORMAT_HTML) {
        global $CFG, $DB;
        // Object to return.
        $retobject = array(
            'success' => true,
            'data' => '',
        );

        if (empty($newname)) {
            $retobject['success'] = false;
            $retobject['data'] = get_string('categorynamecantbeblank', 'question');;
            return $retobject;
        }

        // Get the record we are updating.
        $oldcat = $DB->get_record('question_categories', array('id' => $updateid));
        $lastcategoryinthiscontext = question_is_only_toplevel_category_in_context($updateid);

        if (!empty($newparent) && !$lastcategoryinthiscontext) {
            list($parentid, $tocontextid) = explode(',', $newparent);
        } else {
            $parentid = $oldcat->parent;
            $tocontextid = $oldcat->contextid;
        }

        // Check permissions.
        $fromcontext = context::instance_by_id($oldcat->contextid);
        require_capability('moodle/question:managecategory', $fromcontext);

        // If moving to another context, check permissions some more, and confirm contextid,stamp uniqueness.
        $newstamprequired = false;
        if ($oldcat->contextid != $tocontextid) {
            $tocontext = context::instance_by_id($tocontextid);
            require_capability('moodle/question:managecategory', $tocontext);

            // Confirm stamp uniqueness in the new context. If the stamp already exists, generate a new one.
            if ($DB->record_exists('question_categories', array('contextid' => $tocontextid, 'stamp' => $oldcat->stamp))) {
                $newstamprequired = true;
            }
        }

        // Update the category record.
        $cat = new stdClass();
        $cat->id = $updateid;
        $cat->name = $newname;
        $cat->info = $newinfo;
        $cat->infoformat = $newinfoformat;
        $cat->parent = $parentid;
        $cat->contextid = $tocontextid;
        if ($newstamprequired) {
            $cat->stamp = make_unique_id_code();
        }
        $DB->update_record('question_categories', $cat);

        // If the category name has changed, rename any random questions in that category.
        if ($oldcat->name != $cat->name) {
            $where = "qtype = 'random' AND category = ? AND " . $DB->sql_compare_text('questiontext') . " = ?";

            $randomqtype = question_bank::get_qtype('random');
            $randomqname = $randomqtype->question_name($cat, false);
            $DB->set_field_select('question', 'name', $randomqname, $where, array($cat->id, '0'));

            $randomqname = $randomqtype->question_name($cat, true);
            $DB->set_field_select('question', 'name', $randomqname, $where, array($cat->id, '1'));
        }

        if ($oldcat->contextid != $tocontextid) {
            // Moving to a new context. Must move files belonging to questions.
            question_move_category_to_context($cat->id, $oldcat->contextid, $tocontextid);
        }

        return $retobject;
    }

    /**
     * Creates a new category with given params and return json object
     */
    public function ajax_add_category($newparent, $newcategory, $newinfo, $newinfoformat = FORMAT_HTML) {
        global $DB;
        // Object to return.
        $retobject = array(
            'success' => true,
            'data' => '',
        );

        if (empty($newcategory)) {
            $retobject['success'] = false;
            $retobject['data'] = get_string('categorynamecantbeblank', 'question');;
            return $retobject;
        }
        list($parentid, $contextid) = explode(',', $newparent);
        // Makes sure select element output is legal no need for further cleaning.
        require_capability('moodle/question:managecategory', context::instance_by_id($contextid));

        if ($parentid) {
            if (!($DB->get_field('question_categories', 'contextid', array('id' => $parentid)) == $contextid)) {
                $retobject['success'] = false;
                $retobject['data'] = get_string('cannotinsertquestioncatecontext', 'question');;
                return $retobject;
            }
        }

        $cat = new stdClass();
        $cat->parent = $parentid;
        $cat->contextid = $contextid;
        $cat->name = $newcategory;
        $cat->info = $newinfo;
        $cat->infoformat = $newinfoformat;
        $cat->sortorder = 999;
        $cat->stamp = make_unique_id_code();
        $categoryid = $DB->insert_record("question_categories", $cat);

        // Log the creation of this category.
        $params = array(
            'objectid' => $categoryid,
            'contextid' => $contextid
        );
        $event = \core\event\question_category_created::create($params);
        $event->trigger();

        return $retobject;
    }
}