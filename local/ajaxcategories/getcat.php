<?php
// This file is part of ajaxcategories plugin - https://code.google.com/p/oasychev-moodle-plugins/
//
// Ajaxcategories plugin is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Change list in database after changing in user inreface.
 *
 * @package    local_ajaxcategories
 * @copyright  2015 Oleg Sychev, Volgograd State Technical University
 * @author     Anh Cao
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define('AJAX_SCRIPT', true);

require_once("../../config.php");
require_once($CFG->dirroot."/question/editlib.php");
require_once($CFG->dirroot."/local/ajaxcategories/category_class.php");


list($thispageurl, $contexts, $cmid, $cm, $module, $pagevars) = question_edit_setup(
    'categories', '/local/ajaxcategories/index.php');

// Get values from form for actions on this page.
$param = new stdClass();

$param->delete = optional_param('delete', 0, PARAM_INT);
$param->edit = optional_param('edit', 0, PARAM_INT);

// Create new question category object.
$qcobject = new ajax_question_category_object($pagevars['cpage'], $thispageurl,
    $contexts->having_one_edit_tab_cap('categories'), $param->edit,
    $pagevars['cat'], $param->delete, $contexts->having_cap('moodle/question:add'));

if ($param->edit) {
    $category = $DB->get_record("question_categories", array("id" => $param->edit));
    $category->parent = "{$category->parent},{$category->contextid}";
    $category->submitbutton = get_string('savechanges');
    $category->categoryheader = $qcobject->str->edit;
    $qcobject->ajaxcatform->set_data($category);
}

echo $qcobject->ajaxcatform->display();